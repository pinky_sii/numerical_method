# Numerical method #
### Tri 2/2017 - 2018 ###

* Week 01: Plotting and Taylor Series
* Week 02: Finding Root
* Week 03: Newton's Method and Finding Derivative
* Week 04: Interpolation and Integration
* Week 05: Linear Regression
* Week 06: System of Equatioans
* Week 07: Gradient Descent
* Week 08: MOnte Carlo Simulations
* Week 09: Hill Climbing and ODE
