//////////////////////////////
//STEREOGRAPHICAL PROJECTION//
//////////////////////////////

/// DEFINES ///
outer_r = 100;
inner_r = 96;
northpole = 200;
step=400;
p1=[0,0];
p2=[100,100];


/// RENDERS ///
makelampshade();

/// MODULES ///
//module shell(){
  //   difference(){
        //outside shape 
    //    sphere(r=outer_r, $fn=50);
        //take away inside shape 
      //  sphere(r=inner_r, $fn=50);  
        //cube(size=180);
//    }
//}

module line(p1,p2,w) {
    hull() {
        translate(p1) circle(r=w,$fn=20);
        translate(p2) circle(r=w,$fn=20);
    }
}
module grid(range,step,w){
    for(x=[-range/2:step:range/2]){
        line([x,-range/2],[x,range/2],w);
    }
    for(y=[-range/2:step:range/2]){
        line([-range/2,y],[range/2,y],w);
    }
}


module pattern(){
    intersection(){
        translate([0,0,outer_r+3]) 
            difference(){
                sphere(r=outer_r, $fn=100);  
                sphere(r=inner_r, $fn=100);  
            }
      
        translate([0,0,-5])
        #linear_extrude(height = northpole, center = false, convexity = 5, scale=0.01)
         grid(step,50,5);
    } 
}
module base() {
    translate([0,0,2.5]) cylinder(h = 6, r1 = 20, r2 = 20, center = true, $fn=100);
}

module makelampshade() {
    union(){
      translate([0,0,0]) pattern();
      #base();
    }
}

