/// DEFINE ///
outer_r = 10;
inner_r = 9.7;
northpole = 25;

/// MAIN ////
translate([0,0,outer_r]) extrude();
//shell();
    
    
/// MODULES ///
module shell(){
        difference(){
            //outside shape 
            sphere(r=outer_r, $fn=20);
            //cylinder(h = 50, r1 = outer_r, r2 = outer_r, center = true);
            //take away inside shape 
            sphere(r=inner_r, $fn=20);  
           // cylinder(h = 50, r1 = inner_r, r2 = inner_r, center = true);
        }
}

module extrude() {
  intersection(){
        translate([0,0,0]) shell();
        #linear_extrude(height = northpole, center = true, convexity = 5, scale=0.01)
            translate([-30,-40,0])
            import(file = "square.dxf",convexity=5);
    }
}