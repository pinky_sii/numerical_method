//////////////////////////////
//STEREOGRAPHICAL PROJECTION//
//////////////////////////////

/// DEFINES ///
outer_r = 100;
inner_r = 98;
northpole = [0,0,outer_r];

/// RENDERS ///
translate([0,0,outer_r])
    shell();
2Dplane();

/// MODULES ///

module shell(){
     difference(){
            //outside shape 
            sphere(r=outer_r, $fn=100);
            //take away inside shape 
            sphere(r=inner_r, $fn=100);  
            cube(size=180);
    }
}

module 2Dplane(){
   square([400,400],center=true);
} 



