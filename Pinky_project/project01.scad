/// DEFINES ///
outer_r = 13;
inner_r = 12.4;
northpole = 26;

/// MAIN ////
makelampshade();
 

/// MODULES ///
module shell(){
     difference(){
        //outside shape 
        sphere(r=outer_r, $fn=50);
        //take away inside shape 
        sphere(r=inner_r, $fn=50);  
    }
}
module pattern() {
    intersection(){
      translate([0,0,0.5]) shell();
      linear_extrude(height = northpole-0.9, center = true, convexity = 5, scale=0.01)
            translate([-30,-41.5,0])
            import(file = "pattern07.dxf",convexity=5);
    }
}

module base() {
    //translate([0,0,1]) square(size = 5, center = true);
    translate([0,0,0.65]) cylinder(h = .5, r1 = 3, r2 = 3, center = true, $fn=50);
}

module makelampshade() {
    union(){
      translate([0,0,outer_r]) pattern();
      translate([0,0,-0.02]) base();
    }
}